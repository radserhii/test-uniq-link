<?php

namespace App\Jobs;

use App\HashLink;
use App\Services\PageLinkService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeactivateHashLink implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @param PageLinkService $pageLinkService
     * @return void
     */
    public function handle(PageLinkService $pageLinkService)
    {
        $hashLinks = HashLink::where('is_active', 1)
            ->where('created_at', '<=', now()->subDays(HashLink::LIFE_TIME_DAYS))->get();

        foreach ($hashLinks as $item) {
            $pageLinkService->deactivate($item->hash);
        }
    }
}
