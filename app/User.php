<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'username', 'phone_number'
    ];

    public function hashLink()
    {
        return $this->hasOne('App\HashLink');
    }

    public function lotteries()
    {
        return $this->hasMany('App\Lottery');
    }
}
