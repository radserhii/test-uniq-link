<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HashLink extends Model
{
    public const LIFE_TIME_DAYS = 7;

    protected $fillable = ['hash'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
