<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lottery extends Model
{
    protected $fillable = ['num', 'result', 'sum', 'user_id'];

    public function user()
    {
        $this->belongsTo('App\User');
    }
}
