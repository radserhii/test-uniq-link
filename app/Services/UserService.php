<?php

namespace App\Services;

use App\User;

class UserService
{
    /**
     * Get user by hash link
     *
     * @param string $hash
     * @return User
     */
    public function getUserByHashLink(string $hash): User
    {
        return User::whereHas('hashLink', function ($query) use ($hash) {
            $query->where('hash', $hash);
        })->first();
    }
}
