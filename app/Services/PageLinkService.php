<?php

namespace App\Services;

use App\HashLink;

class PageLinkService
{
    /**
     * Generate uniq link by hash for user to page access
     *
     * @param string $hash
     * @return string
     */
    public function generateLinkByHash(string $hash): string
    {
        return url('/page/' . $hash);
    }

    /**
     * Generate uniq hash
     *
     * @param string $prefix
     * @return string
     */
    public function generateHash(string $prefix = ''): string
    {
        return uniqid();
    }

    /**
     * Check is active hash link
     *
     * @param string $hash
     * @return bool
     */
    public function checkIsActiveHash(string $hash): bool
    {
        return (boolean) HashLink::where('hash', $hash)->first()['is_active'];
    }

    /**
     * Deactivate hash link
     *
     * @param string $hash
     */
    public function deactivate(string $hash): void
    {
        $hashLink = HashLink::where('hash', $hash)->first();
        $hashLink->is_active = 0;
        $hashLink->save();
    }
}
