<?php

namespace App\Services;

use App\Lottery;
use Illuminate\Database\Eloquent\Collection;

class LotteryService
{
    /**
     * Do lottery process
     *
     * @return array
     */
    public function process(): array
    {
        $num = $this->getRandomNumber();
        $isWin = $this->isWin($num);
        return [
            'num' => $num,
            'result' => $isWin ? 'Win' : 'Loss',
            'sum' => $isWin ? number_format($this->getWinSum($num), 2) : null
        ];
    }

    /**
     * Get last lottery history for user
     *
     * @param int $userId
     * @param int $limit
     * @return Collection
     */
    public function getHistory(int $userId, int  $limit): Collection
    {
        return Lottery::where('user_id', $userId)->latest()->take($limit)->get();
    }

    /**
     * Get random number
     *
     * @return int
     */
    private function getRandomNumber(): int
    {
        return rand(0, 1000);
    }

    /**
     * Check is user win
     *
     * @param int $num
     * @return bool
     */
    private function isWin(int $num): bool
    {
        return $num % 2 === 0;
    }

    /**
     * Calculate the winning sum
     *
     * @param int $num
     * @return float
     */
    private function getWinSum(int $num)
    {
        if($num > 900) return $num * 0.7;
        if($num > 601 && $num <= 900) return $num * 0.5;
        if($num > 301 && $num <= 600) return $num * 0.3;
        if($num <= 300) return $num * 0.1;
    }
}
