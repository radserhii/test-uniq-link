<?php

namespace App\Http\Controllers;

use App\Services\PageLinkService;
use App\Services\UserService;

class LinkController extends Controller
{
    private $userService;
    private $pageLinkService;

    public function __construct(UserService $userService, PageLinkService $pageLinkService)
    {
        $this->userService = $userService;
        $this->pageLinkService = $pageLinkService;
    }

    public function generate($hash)
    {
        $user = $this->userService->getUserByHashLink($hash);
        $newHash = $this->pageLinkService->generateHash();
        $user->hashLink()->delete();
        $user->hashLink()->create(['hash' => $newHash]);
        return successResponse(['link' => $this->pageLinkService->generateLinkByHash($newHash)]);
    }

    public function deactivate($hash)
    {
        $this->pageLinkService->deactivate($hash);
        return successResponse([
            'message' => 'Link ' . $this->pageLinkService->generateLinkByHash($hash) . ' was deactivated'
        ]);
    }
}
