<?php

namespace App\Http\Controllers;

use App\Services\PageLinkService;

class PageController extends Controller
{
    private $pageLinkService;

    public function __construct(PageLinkService $pageLinkService)
    {
        $this->pageLinkService = $pageLinkService;
    }

    public function index($hash)
    {
        $link = $this->pageLinkService->generateLinkByHash($hash);
        return view('page', compact('hash', 'link'));
    }
}
