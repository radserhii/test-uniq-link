<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRegistrationRequest;
use App\Services\PageLinkService;
use App\User;

class RegistrationController extends Controller
{
    private $pageLinkService;

    public function __construct(PageLinkService $pageLinkService)
    {
        $this->pageLinkService = $pageLinkService;
    }

    public function index()
    {
        return view('registration');
    }

    public function registration(UserRegistrationRequest $request)
    {
        $user = User::create($request->all());
        $hash = $this->pageLinkService->generateHash();
        $user->hashLink()->create(compact('hash'));
        return successResponse(['link' => $this->pageLinkService->generateLinkByHash($hash)]);
    }
}
