<?php

namespace App\Http\Controllers;

use App\Http\Resources\LotteryHistoryResource;
use App\Services\LotteryService;
use App\Services\UserService;

class LotteryController extends Controller
{
    private $userService;
    private $lotteryService;

    public function __construct(UserService $userService, LotteryService $lotteryService)
    {
        $this->userService = $userService;
        $this->lotteryService = $lotteryService;
    }

    public function process($hash)
    {
        $user = $this->userService->getUserByHashLink($hash);
        $data = $this->lotteryService->process();
        $user->lotteries()->create($data);
        return successResponse($data);
    }

    public function getHistory($hash)
    {
        $user = $this->userService->getUserByHashLink($hash);
        $data = $this->lotteryService->getHistory($user->id, 3);
        return successResponse(LotteryHistoryResource::collection($data));
    }
}
