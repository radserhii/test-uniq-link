<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRegistrationRequest;
use App\User;

class UserController extends Controller
{
    public function getAll()
    {
        return successResponse(User::all());
    }

    public function create(UserRegistrationRequest $request)
    {
        $user = User::create($request->all());
        return successResponse($user);
    }

    public function update(UserRegistrationRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
        return successResponse($user->fresh());
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        return successResponse($user->delete());
    }
}
