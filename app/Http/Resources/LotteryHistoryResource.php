<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LotteryHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'num' => $this->num,
            'result' => $this->result,
            'sum' => $this->sum,
            'date' => $this->created_at->format('Y-m-d H:i:s')
        ];
    }
}
