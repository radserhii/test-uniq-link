<?php

namespace App\Http\Middleware;

use App\Services\PageLinkService;
use Closure;

class CheckActiveLink
{
    private $pageLinkService;

    public function __construct(PageLinkService $pageLinkService)
    {
        $this->pageLinkService = $pageLinkService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $hash = $request->route()->parameter('hash');
        if(!$this->pageLinkService->checkIsActiveHash($hash))
            abort(403, 'Page link is out of date');
        return $next($request);
    }

}
