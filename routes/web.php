<?php

Route::get('/', 'RegistrationController@index')->name('registration');
Route::post('registration', 'RegistrationController@registration');

Route::group(['middleware' => 'link.active'], function () {
    Route::get('page/{hash}', 'PageController@index');
    Route::post('link/{hash}', 'LinkController@generate');
    Route::put('link/{hash}', 'LinkController@deactivate');
    Route::get('lottery/{hash}', 'LotteryController@process');
    Route::get('history/{hash}', 'LotteryController@getHistory');
});

Route::get('admin', 'AdminController@index')->name('admin.index');

Route::get('users', 'UserController@getAll');
Route::post('user', 'UserController@create');
Route::put('user/{id}', 'UserController@update');
Route::delete('user/{id}', 'UserController@delete');

