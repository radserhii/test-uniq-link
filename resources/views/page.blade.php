@extends('layouts.app')

@section('content')
    <page-component hash="{{$hash}}" link="{{$link}}"></page-component>
@endsection
