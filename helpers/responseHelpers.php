<?php

if (!function_exists("successResponse")) {
    /**
     * @param $data
     * @param int $status
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    function successResponse($data, $status = 200, $headers = [])
    {
        if (is_string($data)) {
            $data = [$data];
        }

        $response = [
            'data' => $data,
            'status' => $status === 200 ? 1 : 0,
        ];

        return response()->json($response, $status, $headers);
    }
}

if (!function_exists("errorResponse")) {
    /**
     * @param $errors
     * @param int $status
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    function errorResponse($errors, $status = 400, $headers = [])
    {
        if (is_string($errors)) {
            $errors = [$errors];
        }

        $response = [
            'status' => 0,
            'errors' => $errors
        ];

        return response()->json($response, $status, $headers);
    }
}
